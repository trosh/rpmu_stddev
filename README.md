# RPMU median standard deviation estimator

Estimate standard deviation of median values
for the “random permutation max usage” metric
provided by ibnet's `--msd`:

* Routing tables are computed once using fmpoc
  for a given topology, degradation, and algorithm;
* ibnet is called repeatedly to compute median RPMU
  with the given number of random permutations,
  showing up-to-date variance and standard deviation
  after each new throw.

Requires Python 2, fmpoc, ibnet.

Configure in file and simply call with:

    ./rpmu_stddev.py
