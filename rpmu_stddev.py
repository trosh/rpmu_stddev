#!/usr/bin/env python2
# vim: tabstop=4 shiftwidth=4 noexpandtab

from __future__ import print_function
from subprocess import check_output

FMPOC_CMD  = "fmpoc"
TOPOLOGY   = "bigtopo.bxi"
ALGORITHM  = "dmodc"
DEG_AMOUNT = 256
DEG_FLAG   = "-S"
IBNET_CMD  = "ibnet"
THROWS     = 100
RAND_STEPS = 1000

# Compute routing tables
print("Degrade {} ({} {}) and compute {} routing tables...".format(
	TOPOLOGY, DEG_FLAG, DEG_AMOUNT, ALGORITHM))
check_output([
	FMPOC_CMD, "-f", TOPOLOGY,
	"-a", ALGORITHM,
	DEG_FLAG, str(DEG_AMOUNT),
	"-d", "-z",
])

print("Compute {}-throw RPMU repeatedly and make stats...".format(
	RAND_STEPS))
print("n=1\t...\r", end="")
rpmus = list()
for throw in range(THROWS):
	ibnet_cmd = [
		IBNET_CMD, "-d", "network",
		"--cleanup", "--msd",
		"--msd_rand_steps", str(RAND_STEPS),
		"--msd_quantile", "2"
	]
	ibnet_out = check_output(ibnet_cmd)
	for line in ibnet_out.strip().split("\n"):
		if "RPMU median = " in line:
			rpmu = int(line.rsplit(' ', 1)[1])
			break
	else:
		print("could not find RPMU. continue.")
		continue
	rpmus.append(rpmu)
	n = len(rpmus)
	avg = sum(rpmus) / n
	var = float(sum(map(lambda x: (x - avg) ** 2, rpmus))) / n
	stddev = var ** .5
	print("n={}\tlastrpmu={}\tvariance={:.3f}\tstddev={:.3f}".format(
		n, rpmus[-1], var, stddev))
	print("n={}\t...\r".format(n + 1), end="")
print("\033[K", end="")
